/*
* convertVideoToCSV.cpp
*
*  Created on: June 26 2018
*      Author: Jordan Tritell
*
* Converts a video file input to a CSV with timetamped, fixed size frames
*/

#include <iostream>
#include <fstream>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;
using namespace std;

int convertVideoToCSV(VideoCapture& capture, string output_filename, int keyframe, int output_dim_x = 64, int output_dim_y = -1) {
    if (output_dim_y == -1) output_dim_y = output_dim_x;
    ofstream os(output_filename, ofstream::out);

    Mat input_frame;
    Mat grayscale_frame;

    capture.set(CV_CAP_PROP_POS_FRAMES, keyframe);
    double timestamp = capture.get(CAP_PROP_POS_MSEC);
    int input_dim_x = capture.get(CAP_PROP_FRAME_WIDTH);
    int input_dim_y = capture.get(CAP_PROP_FRAME_HEIGHT);
    
    
    bool read_success = capture.read(input_frame); // read a new frame from video
    if (read_success == false) {
      cerr << "Failed to open frame "<< keyframe<< "!\n" << endl;
      return 1;
    }
    if (input_frame.empty()) {
      cerr << "Requested frame is empty !\n" << endl;
      return 1;
    }
    
    // convert to greyscale
    cvtColor(input_frame, grayscale_frame, CV_RGB2GRAY);
    // flush to output file

    // convert timestamp ms to seconds
    os << 0.001f * timestamp;
    for (int col = 0; col<output_dim_y; ++col) {
      for (int row = 0; row<output_dim_x; ++row) {	
	// Include border pixels when scaling down
	int px_x = floor(1.0f * row * input_dim_x / output_dim_x);
	int px_y = floor(1.0f * col * input_dim_y / output_dim_y);
	int px_x_stop = ceil(1.0f * (row + 1) * input_dim_x / output_dim_x);
	int px_y_stop = ceil(1.0f * (col + 1) * input_dim_y / output_dim_y);
	int sample_width = px_x_stop - px_x;
	int sample_height = px_y_stop - px_y;

	vector<uchar> pixels(sample_width * sample_height);
	for(int y = px_y;y < px_y_stop; y++) {
	  for(int x = px_x;x < px_x_stop; x++) {
	    pixels[(x-px_x) + (y-px_y) * sample_height] = grayscale_frame.at<uchar>(y,x);
	  }
	}
	// take median
	sort(pixels.begin(),pixels.end());
	float median = pixels[pixels.size()/2];
	if(pixels.size() % 2==0) {
	  median += pixels[pixels.size()/2 + 1];
	  median /= 2;
	}
	// Write value into CSV
	os << "," << median;
      }
    }
	
    os.close();
    return 0;
}

    int main(int argc, char** argv) {

    if (argc < 4) {
        cerr << "Not enough arguments. Expects : convertToCSV input_file output_file keyframe dimension_x dimension_y\n" << endl;
        return 1;
    }
    string in_file = argv[1];
    string out_file = argv[2];

    int keyframe = stoi(argv[3]);
    
    int output_dimensions_x = (argc < 5) ? 64 : stoi(argv[4]);
    int output_dimensions_y = (argc < 6) ? output_dimensions_x : stoi(argv[5]);

    VideoCapture capture(in_file); //try to open string, this will attempt to open it as a video file
    if (!capture.isOpened()) {
        cerr << "Failed to open a video file!\n" << endl;
        // help(av);
        return 1;
    }
    return convertVideoToCSV(capture, out_file, keyframe, output_dimensions_x, output_dimensions_y);
}
