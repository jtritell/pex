# Project Objective
Given a video file and dimensions (e.g. 32x32, 64x64, 128x128), extract keyframes from the video, convert the frame into grayscale, split each frame into a grid of said dimensions, calculate median value of all the pixels of each cell of the grid and write the values to a CSV file together with the timestamp of the frame.
# Requirements (Linux)
* g++
* cmake
* opencv

# Build Steps
```
cmake .
make
```

# How To Run
```
./convertVideoToCSV video_in csv_out keyframe dimension_x dimension_y
```
## Arguments
*  **video_in** : video file input to convert to csv table
* **csv_out** : csv file to output frame information
* **keyframe** : zero indexed keyframe to parse
* **dimension_x** : image columns in csv frame data
* **dimension_y** : image rows in csv frame data